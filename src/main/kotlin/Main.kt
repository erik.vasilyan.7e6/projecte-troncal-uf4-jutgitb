@file:Suppress("SpellCheckingInspection")

import java.util.*

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-02-10
    * TITLE: Jutge de codi
 */

const val defaultColor = "\u001B[0m"
const val red = "\u001B[31m"
const val green = "\u001B[32m"

class Problema(private val enunciat: String, private val jocDeProvesPublic: JocDeProves, val jocDeProvesPrivat: JocDeProves, var resolt: Boolean = false, var intents: Int, var llistaIntents: MutableList<Intent>) {

    fun mostrarProblema() {
        println(enunciat)
        println("\nEntrada: ${jocDeProvesPublic.input}")
        println("\nSortida: ${jocDeProvesPublic.output}")
    }

    fun mostrarNombreDeIntents() {
        println("\nNombre dels intents: $intents")
    }

    fun mostrarElsIntentsFets() {
        println("\nIntents: $llistaIntents")
    }

    fun afegirIntent(intent: Intent) {
        llistaIntents.add(intent)
    }

    fun sumarUnIntent() {
        intents += 1
    }
}

data class JocDeProves (val input: String, val output: String)
data class Intent(val resposta: String)

fun main() {
    val scanner = Scanner(System.`in`)

    var problemesResolts = 0

    val llistaDeProblemes = mutableListOf(
        Problema("Per a una frase d'entrada determinada, trobeu la paraula més llarga de la frase.", JocDeProves("La intricada i fascinant complexitat dels ecosistemes naturals ens recorda la fragilitat de la vida.", "complexitat"), JocDeProves("Quan el sol es pon al horitzó i les estrelles apareixen en el cel nocturn, em sento petit davant la immensitat de l'univers.", "immensitat"), false, 0, mutableListOf()),
        Problema("Donada una matriu d'enters, escriu un programa que pugui trobar la suma de tots els elements de la matriu.", JocDeProves("[[1, 2, 3], [4, 5, 6], [7, 8, 9]]", "45"), JocDeProves("[[91, 45, 23], [76, 48, 55], [81, 37, 18]]", "474"), false, 0, mutableListOf()),
        Problema("Escriu un programa que donada una cadena de caràcters, compti el nombre de paraules que hi ha a la cadena.", JocDeProves("El gosset corre pel jardí", "5"), JocDeProves("El gat s'ha quedat atrapat dins la caixa i no pot sortir. Estic intentant ajudar-lo però no sembla que vulgui cooperar. A vegades els gats són així, tancats en el seu propi món. Si us plau, gatet, ajuda'm a ajudar-te. Oh, sembla que ha trobat un forat i ha sortit per fi. Què faria jo sense aquest petit amic pelut?", "87"), false, 0, mutableListOf()),
        Problema("Escriu un programa que demani a l'usuari una paraula i retorni el nombre de vocals que conté.", JocDeProves("elefant", "3"), JocDeProves("anticonstitucionalíssimament", "12"), false, 0, mutableListOf()),
        Problema("Escriu un programa que demani a l'usuari una llista de nombres enters i retorni el nombre màxim de la llista.", JocDeProves("[4, 9, 2, 6, 1]", "9"), JocDeProves("[34, 16, 27, 45, 12, 87, 56, 39, 29, 71, 22, 11, 64, 43, 76, 51, 90, 47, 18, 55, 32, 77, 28, 80, 60, 21, 73, 95, 65, 24]", "95"), false, 0, mutableListOf()),
    )
    llistaDeProblemes.shuffle()

    print(">>> Jutge <<<\n")
    repeat(14) { print("-") }
    println("\n")

    for (problema in llistaDeProblemes) {

        problema.mostrarProblema()

        print("\nVols resoldre aquest problema? [SI / NO]: ")
        var opcio = scanner.next().uppercase()

        while (opcio == "SI") {

            if (problema.intents > 0) problema.mostrarNombreDeIntents()
            if (problema.llistaIntents.isNotEmpty()) problema.mostrarElsIntentsFets()

            println("\nEntrada: ${problema.jocDeProvesPrivat.input}")
            print("\nSortida: ")
            val respostaUsuari = scanner.next()

            val intent = Intent(respostaUsuari)
            problema.sumarUnIntent()
            problema.afegirIntent(intent)

            if (respostaUsuari == problema.jocDeProvesPrivat.output) {
                println("\n${green}Resposta correcta!$defaultColor")
                problema.resolt = true
                problemesResolts += 1
                opcio = "NO"
            }
            else {
                println("\n${red}Resposta incorrecta!$defaultColor")
                print("\nVols intentar una vegada més? [SI / NO]: ")
                opcio = scanner.next().uppercase()
            }
        }
        repeat(45) { print("-") }
        println("\n")
    }
    println("Problemes resolts: $problemesResolts")
}